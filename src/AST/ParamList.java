package AST;

import java.util.ArrayList;

public class ParamList {
    private ArrayList<ParamDec> arrayParamDec;

    // construtor sem parâmetros
    public ParamList() {
        this.arrayParamDec = new ArrayList<ParamDec>();
    }

    // construtor com parametros
    public ParamList(ArrayList<ParamDec> arrayParamDec) {
        this.arrayParamDec = arrayParamDec;
    }

    // para adicionar um parâmetro na lista de parâmetros
    public void addParam(ParamDec paramDec) {
        this.arrayParamDec.add(paramDec);
    }

    public ArrayList<ParamDec> getArrayParamDec() {
        return arrayParamDec;
    }

    /**
     * Geração de código em C
     */
	public void genc(PW pw) {
        ArrayList<ParamDec> paramDecList = this.arrayParamDec;
        if (paramDecList.size() > 0) {

            // Percorre cada declaração de variável
            int i = 0;
            for (ParamDec paramDec : paramDecList) {
                i++;
                paramDec.genC(pw);

                // Separa por vírgula se não for o último
                if (i != paramDecList.size()) {
                    pw.print(", ");
                }
            }

        }
	}
}
