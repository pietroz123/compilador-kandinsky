package AST;

abstract public class Type {
    public Type(String name) {
        this.name = name;
    }

    public static Type booleanType = new TypeBoolean();
    public static Type integerType = new TypeInteger();
    public static Type stringType = new TypeString();
    public static Type undefinedType = new TypeUndefined();
    public static Type voidType = new TypeVoid();

    public String getName() {
        return name;
    }

    abstract public String getCname();
    abstract public String getCspecifier();

    private String name;
}
