package AST;

import Lexer.Symbol;

/**
 * ExprUnary ::= [ ( "+" | "-" ) ] ExprPrimary
 */
public class ExprUnary {
    private Symbol op;
    private ExprPrimary exprPrimary;

    public ExprUnary(Symbol op, ExprPrimary exprPrimary) {
        this.op = op;
        this.exprPrimary = exprPrimary;
    }

    public Type getType() {
        return op != null ? Type.integerType : exprPrimary.getType();
    }

    /**
     * Geração de código em C
     * @param pw
     */
    public void genC(PW pw) {
        if (this.op != null) {
            pw.out.print(op.toString());
        }

        this.exprPrimary.genC(pw);
    }
}
