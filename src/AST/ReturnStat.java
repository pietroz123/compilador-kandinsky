package AST;

/**
 * ReturnStat ::= "return" Expr ";"
 */
public class ReturnStat extends Stat {
    private Expr e;

    public ReturnStat(Expr e) {
        this.e = e;
    }

    @Override
    public void genC(PW pw) {
        pw.print("return ");
        this.e.genC(pw);
        pw.out.print(";");
    }
}
