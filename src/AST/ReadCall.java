package AST;

/**
 * scanf("especificador", &nomeVar)
 */
public class ReadCall extends FuncCall {

    public ReadCall(Variable currentVarible) {
        super(currentVarible);
    }

    /**
     * Geração de código em C
     * @param pw
     */
    public void genC(PW pw) {
        // Inicio do scanf
        pw.print("scanf(\"");

        // Imprime especificador : %d, %s, ...
        pw.out.print(super.getVariable().getType().getCspecifier());

        // Fecha especificador
        pw.out.print("\", &");

        // Imprime nome da variável
        pw.out.print(super.getVariable().getName());

        // Fecha o scanf
        pw.out.print(")");
    }

}
