package AST;

public class LiteralBoolean extends ExprLiteral {
	public static LiteralBoolean True = new LiteralBoolean(true);
	public static LiteralBoolean False = new LiteralBoolean(false);
	private boolean value;

	public LiteralBoolean(boolean value) {
		this.value = value;
	}

	public Type getType() {
		return Type.booleanType;
	}

	public void genC(PW pw) {
		pw.out.print(value ? "true" : "false");
	}
}
