package AST;

import java.util.ArrayList;

public class StatList {
    private ArrayList<Stat> arrayStat;

    public StatList() {
        this.arrayStat = new ArrayList<Stat>();
    }

    public StatList(ArrayList<Stat> arrayStat) {
        this.arrayStat = arrayStat;
    }

    public void addStat(Stat stat) {
        this.arrayStat.add(stat);
    }

    public ArrayList<Stat> getStatList() {
        return this.arrayStat;
    }

    /**
     * Geração de código em C
     * @param pw
     */
	public void genc(PW pw) {
        ArrayList<Stat> statsList = this.arrayStat;

        for (Stat stat : statsList) {
            stat.genC(pw);
        }
	}
}
