package AST;

import java.util.ArrayList;

public class Program {
    private ArrayList<Func> arrayFunc;

    public Program(ArrayList<Func> arrayFunc) {
        this.arrayFunc = arrayFunc;
    }

    /**
     * Geração de código em C
     */
    public void genC(PW pw) {

        pw.out.println("#include <stdio.h>");
        pw.out.println("#include <stdbool.h>");
        pw.out.println("#include <string.h>");
        pw.out.println("");

        ArrayList<Func> funcList = this.arrayFunc;

        // Percorre cada função
        for (Func func : funcList) {
            func.genC(pw);
        }

    }
}
