package AST;

public class LiteralString extends ExprLiteral {
	private String value;

	public LiteralString(String value) {
		this.value = value;
	}

	public String getValue() {
		return value;
	}

	public Type getType() {
		return Type.stringType;
	}

	public void genC(PW pw) {
		pw.out.print("\"" + value + "\"");
	}
}
