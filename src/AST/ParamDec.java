package AST;

public class ParamDec {
    private Variable var;

    public ParamDec(Variable var) {
        this.var = var;
    }

    public Variable getVar() {
        return var;
    }

    /**
     * Geração de código em C
     * @param pw
     */
	public void genC(PW pw) {
        Variable var = this.var;
        pw.print(var.getType().getCname());
        pw.print(" ");
        pw.print(var.getName());
	}
}
