package AST;

public class Func {
    private String id;
    private ParamList paramList = new ParamList();
    private StatList statList = new StatList();
    private Type type;

    public Func(String id, ParamList paramList, Type type, StatList statList) {
        this.id = id;
        this.paramList = paramList;
        this.type = type;
        this.statList = statList;
    }

    public Func(String id) {
        this.id = id;
    }

    public void setReturnType(Type type) {
        this.type = type;
    }

    public void setParamList(ParamList paramList) {
        this.paramList = paramList;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Type getReturnType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public ParamList getParamList() {
        return paramList;
    }

    /**
     * Geração de código em C
     */
    public void genC(PW pw) {

        // Imprime tipo
        pw.out.print(this.type != null ? this.type.getCname() : "void");
        pw.out.print(" ");

        // Imprime nome
        pw.out.print(this.id);

        // Abre parênteses
        pw.out.print(" (");

        // Se existirem parâmetros
        if (this.paramList != null) {
            this.paramList.genc(pw);
        }

        // Fecha parênteses
        pw.out.print(") ");

        // Início do corpo da função
        pw.out.print(" {\n");
        pw.add();

        // Se existirem statements
        if (this.statList != null) {
            this.statList.genc(pw);
        }

        pw.sub();
        pw.print("\n}\n");

    }
}
