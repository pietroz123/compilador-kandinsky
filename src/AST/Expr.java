package AST;

import java.util.ArrayList;

/**
 * Expr ::= ExprAnd { "||" ExprAnd }
 */
public class Expr {
    private ArrayList<ExprAnd> array;

    public Expr(ArrayList<ExprAnd> array) {
        this.array = array;
    }

    public Type getType() {
        if (array == null || array.size() == 0)
            return Type.undefinedType;

        return array.size() > 1 ? Type.booleanType : array.get(0).getType();
    }

    /**
     * Geração de código em C
     * @param pw
     */
    public void genC(PW pw) {
        int i = 0;
        this.array.get(i).genC(pw);
        i++;

        while (i < this.array.size()) {
            pw.out.print(" || ");
            this.array.get(i).genC(pw);
            i++;
        }
    }
}
