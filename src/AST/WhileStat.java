package AST;

/**
 * WhileStat ::= "while" Expr "do" StatList "endw"
 */
public class WhileStat extends Stat {
    private Expr expr;
    private StatList statList;

    public WhileStat(Expr expr, StatList statList) {
        this.expr = expr;
        this.statList = statList;
    }

    @Override
    public void genC(PW pw) {
        pw.print("while (");

        this.expr.genC(pw);

        pw.out.print(") {\n");

        pw.add();
        this.statList.genc(pw);
        pw.sub();

        pw.print("}\n");
    }
}
