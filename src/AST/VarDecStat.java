package AST;

/**
 * VarDecStat ::= "var" Type Id ";"
 */
public class VarDecStat extends Stat {
    private Variable var;

    public VarDecStat(Variable var) {
        this.var = var;
    }

    @Override
    public void genC(PW pw) {
        pw.print(this.var.getType().getCname());
        pw.out.print(" ");
        pw.out.print(this.var.getName());
        pw.out.print(";\n");
    }
}
