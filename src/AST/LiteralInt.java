package AST;

public class LiteralInt extends ExprLiteral {
	private Integer value;

	public LiteralInt(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}

	public Type getType() {
		return Type.integerType;
	}

	/**
	 * Geração de código em C
	 */
	public void genC(PW pw) {
		pw.out.print(this.value.toString());
	}
}
