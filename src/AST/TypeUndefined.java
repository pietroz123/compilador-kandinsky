package AST;

public class TypeUndefined extends Type {

    // variables that are not declared have this type
    public TypeUndefined() {
        super("undefined");
    }

    public String getCname() {
        return "undefined";
    }

    @Override
    public String getCspecifier() {
        return "";
    }
}
