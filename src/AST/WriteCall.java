package AST;

import java.util.ArrayList;

/**
 * print( { Expr } ) || println( { Expr } )
 */
public class WriteCall extends FuncCall {

    public WriteCall(ArrayList<Expr> arrayExpr, String id, Type type) {
        super(arrayExpr, id, type);
    }

    /**
     * Geração de código em C
     * @param pw
     */
    public void genC(PW pw) {
        // Inicia printf
        pw.out.print("printf(\"");

        ArrayList<Expr> exprList = super.getArrayExpr();

        // Imprime especificador : %d, %s, ...
        pw.out.print(exprList.get(0).getType().getCspecifier());

        // Se println, imprime uma quebra de linha
        if (super.getId().equals("println")) {
            pw.out.print("\\n");
        }

        // Fecha especificador
        pw.out.print("\", ");

        // Imprime as expressões
        if (exprList.size() > 0) {
            int i = 0;
            exprList.get(i).genC(pw);
            i++;

            while (i < exprList.size()) {
                pw.out.print(" , ");
                exprList.get(i).genC(pw);
                i++;
            }
        }

        // Fecha o printf
        pw.out.print(")");
    }

}
