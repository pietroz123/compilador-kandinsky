package AST;

abstract public class ExprPrimary {
    abstract public void genC(PW pw);

    abstract public Type getType();
}
