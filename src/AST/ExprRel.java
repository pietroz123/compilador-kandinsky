package AST;

import Lexer.Symbol;

/**
 * ExprRel ::= ExprAdd [ RelOp ExprAdd ]
 */
public class ExprRel {
    private ExprAdd leftExprAdd;
    private ExprAdd rightExprAdd;
    private Symbol op;

    public ExprRel(ExprAdd leftExprAdd, ExprAdd rightExprAdd, Symbol op) {
        this.leftExprAdd = leftExprAdd;
        this.rightExprAdd = rightExprAdd;
        this.op = op;
    }

    public Type getType() {
        if ( op == Symbol.EQ || op == Symbol.NEQ || op == Symbol.LE || op == Symbol.LT || op == Symbol.GE || op == Symbol.GT) {
            return Type.booleanType;
        }

        if (leftExprAdd == null) {
            return Type.undefinedType;
        }

        return leftExprAdd.getType();
    }

    /**
     * Geração de código em C
     * @param pw
     */
    public void genC(PW pw) {
        this.leftExprAdd.genC(pw);

        if (this.rightExprAdd != null) {
            pw.out.print(" " + this.op + " ");
            this.rightExprAdd.genC(pw);
        }
    }
}
