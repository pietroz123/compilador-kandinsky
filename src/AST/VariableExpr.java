package AST;

public class VariableExpr extends ExprPrimary {
    private Variable v;

    public VariableExpr(Variable v) {
        this.v = v;
    }

    public Type getType() {
        return v.getType();
    }

    @Override
    public void genC(PW pw) {
        pw.out.print(v.getName());
    }
}
