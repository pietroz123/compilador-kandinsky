package AST;

public class TypeInteger extends Type {
    public TypeInteger() {
        super("int");
    }

    public String getCname() {
        return "int";
    }

    @Override
    public String getCspecifier() {
        return "%d";
    }
}
