package AST;

/**
 * IfStat ::= "if" Expr "then" StatList [ "else" StatList ] "endif"
 */
public class IfStat extends Stat {
    private Expr expr;
    private StatList leftPart = new StatList();
    private StatList rightPart = new StatList();

    public IfStat(Expr expr, StatList leftPart, StatList rightPart) {
        this.expr = expr;
        this.leftPart = leftPart;
        this.rightPart = rightPart;
    }

    @Override
    public void genC(PW pw) {
        pw.print("if (");
        this.expr.genC(pw);
        pw.out.print(") {\n");

        pw.add();
        this.leftPart.genc(pw);
        pw.sub();

        pw.print("}\n");

        if (this.rightPart != null) {
            pw.print("else {\n");
            pw.add();
            this.rightPart.genc(pw);
            pw.sub();
            pw.print("}\n");
        }
    }
}
