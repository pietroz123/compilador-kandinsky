package AST;

import java.util.ArrayList;

/**
 * FuncCall ::= Id "(" [ Expr { "," Expr } ] ")"
 */
public class FuncCall extends ExprPrimary {
    private ArrayList<Expr> arrayExpr = new ArrayList<Expr>();
    private String id;
    private Type type;
    private Variable var; // só serve para o ReadCall

    public FuncCall(ArrayList<Expr> arrayExpr, String id, Type type) {
        this.arrayExpr = arrayExpr;
        this.id = id;
        this.type = type;
    }

    // só serve para o ReadCall
    public FuncCall(Variable var) {
        this.var = var;
    }

    // só serve para o ReadCall
    public Variable getVariable() {
        return var;
    }

    public String getId() {
        return id;
    }

    public Type getType() {
        return type;
    }

    public ArrayList<Expr> getArrayExpr() {
        return arrayExpr;
    }

    /**
     * Geração de código em C
     * @param pw
     */
    public void genC(PW pw) {
        pw.out.print(this.id);
        pw.out.print(" (");

        if (this.arrayExpr.size() > 0) {
            int i = 0;
            this.arrayExpr.get(i).genC(pw);
            i++;

            while (i < this.arrayExpr.size()) {
                pw.out.print(" , ");
                this.arrayExpr.get(i).genC(pw);
                i++;
            }
        }

        pw.out.print(")");
    }
}
