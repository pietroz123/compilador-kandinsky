package AST;

import java.util.ArrayList;
import Lexer.Symbol;

/**
 * ExprAdd ::= ExprMult { ( "+" | "-" ) ExprMult }
 */
public class ExprAdd {
    private ArrayList<ExprMult> array;
    private ArrayList<Symbol> arrayOp;

    // construtor com parametros
    public ExprAdd(ArrayList<ExprMult> array, ArrayList<Symbol> arrayOp) {
        this.array = array;
        this.arrayOp = arrayOp;
    }

    public Type getType() {
        if (array == null || array.size() == 0)
            return Type.undefinedType;

        return array.size() > 1 ? Type.integerType : array.get(0).getType();
    }

    /**
     * Geração de código em C
     * @param pw
     */
    public void genC(PW pw) {
        int i = 0;
        this.array.get(i).genC(pw);
        i++;

        while (i < this.array.size()) {
            pw.out.print(" " + this.arrayOp.get(i-1).toString() + " ");
            this.array.get(i).genC(pw);
            i++;
        }
    }
}
