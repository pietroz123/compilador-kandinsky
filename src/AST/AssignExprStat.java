package AST;

/**
 * AssignExprStat ::= Expr [ "=" Expr ] ";"
 */
public class AssignExprStat extends Stat {
    private Expr left, right;

    public AssignExprStat(Expr left, Expr right) {
        this.left = left;
        this.right = right;
    }

    @Override
    public void genC(PW pw) {
        if (this.left != null) {
            pw.print("");
            this.left.genC(pw);
        }

        if (this.right != null) {

            if (this.left != null) {
                pw.out.print(" = ");
            }

            this.right.genC(pw);
        }

        pw.out.print(";\n");
    }
}
