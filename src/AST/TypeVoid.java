package AST;

public class TypeVoid extends Type {

    // variables that are not declared have this type
    public TypeVoid() {
        super("void");
    }

    public String getCname() {
        return "void";
    }

    @Override
    public String getCspecifier() {
        return "";
    }
}
