package Error;

import java.io.PrintWriter;
import Lexer.Lexer;

public class CompilerError {
    public CompilerError(PrintWriter out, String inFileName) {
        // output of an error is done in out
        this.out = out;
        this.inFileName = inFileName;
    }

    public void setLexer(Lexer lexer) {
        this.lexer = lexer;
    }

    public void signal(String strMessage) {
        if (out.checkError())
            System.out.println("Error in signaling an error");

        // \n<nome do arquivo>:<número da linha de erro>:<mensagem de erro>\n<linha do código com erro>
        System.out.println("\n"+inFileName+" : "+lexer.getLineNumber()+" : "+strMessage+"\n"+lexer.getCurrentLine());
        System.out.println();

        throw new RuntimeException(strMessage);
    }

    private Lexer lexer;
    private String inFileName;
    PrintWriter out;
}