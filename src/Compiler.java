
/**
 * Nome                        | RA
 * ------------------------------------
 * Pietro Zuntini Bonfim       | 743588
 */

import AST.*;
import AuxComp.SymbolTable;
import Error.CompilerError;
import java.util.*;
import java.io.PrintWriter;
import Lexer.*;

public class Compiler {

    /**
     * Função principal compile
     */
    public Program compile(char[] input, PrintWriter pw, String inFileName) {
        symbolTable = new SymbolTable();
        error = new CompilerError(pw, inFileName);
        lexer = new Lexer(input, error);
        error.setLexer(lexer);
        lexer.nextToken();

        return program();
    }

    /**
     * Métodos dos "non-terminals"
     */

    // Program ::= Func { Func }
    public Program program() {

        ArrayList<Func> arrayFunction = new ArrayList<>();

        // primeiro Func
        arrayFunction.add(func());
        lexer.nextToken();

        // próximos Func
        while (lexer.token == Symbol.DEF) {
            arrayFunction.add(func());
            lexer.nextToken();
        }

        if (lexer.token != Symbol.EOF)
            error.signal("End of file expected");

        // Verifica se encontrou um método main
        if (symbolTable.getInGlobal("func", "main") == null) {
            error.signal("Every program in the Kandinsky language must have a 'main' function");
        }

        Program program = new Program(arrayFunction);

        return program;
    }

    // Func ::= "def" Id [ "(" ParamList ")" ] [ ":" Type ] "{" StatList "}"
    public Func func() {

        ParamList paramList = null;
        Type type = null;
        StatList statList = null;

        /**
         * Uma Func deve ter um identificador "def" seguido de um Id,
         * uma lista de parametros envolvidos em (...), seguido de um tipo,
         * e uma lista de statements envolvidos por {...}
         */

        /**
         * Verifica "def"
         */
        if (lexer.token != Symbol.DEF) {
			if (lexer.token == Symbol.LITERALINT) {
				error.signal("'def' expected before " + lexer.getNumberValue());
			} else if (lexer.token == Symbol.LITERALSTRING || lexer.token == Symbol.IDENT) {
				error.signal("'def' expected before " + lexer.getStringValue());
			} else {
				error.signal("'def' expected before " + lexer.token);
			}
		} else {
			lexer.nextToken();
		}

        /**
         * Verifica "Id"
         */
        if (lexer.token != Symbol.IDENT) {
			if (lexer.token == Symbol.LITERALINT) {
				error.signal("Function Id expected before " + lexer.getNumberValue());
			} else if (lexer.token == Symbol.LITERALSTRING || lexer.token == Symbol.IDENT) {
				error.signal("Function Id expected before " + lexer.getStringValue());
			} else {
                if (lexer.isKeyword(lexer.getStringValue())) {
                    error.signal("Function Id cannot have the same name of a keyword: " +  lexer.getStringValue());
                } else {
                    error.signal("Function Id expected before " + lexer.token);
                }
			}
		} else {
			lexer.nextToken();
        }

        String id = lexer.getStringValue();

        // Verifica se a função já não foi declarada
        if (symbolTable.getInGlobal("func", id) != null) {
            error.signal("Function '"+id+"' has already been declared");
        }

        // Se chegou aqui, então a função tem pelo menos um ID
        // Então inicializa a função atual
        currentFunction = new Func(id);

        /**
         * Verifica [ "(" ParamList ")" ] (opcional)
         */
        if (lexer.token == Symbol.LEFTPAR) {

            // Verifica se a função 'main' não possuí nenhum parãmetro
            if (id.equals("main")) {
                error.signal("The 'main' method must not have any parameters");
            }

            lexer.nextToken();
            paramList = paramList();

            if (lexer.token != Symbol.RIGHTPAR)
                error.signal(") expected");

            lexer.nextToken();

            // Atribui lista de parâmetros
            currentFunction.setParamList(paramList);
        }

        /**
         * Verifica [ ":" Type ] (opcional)
         */
        if (lexer.token == Symbol.COLON) {
            lexer.nextToken();
            type = type();

            // Atribui tipo
            currentFunction.setType(type);
        }

        // A função 'main' tem retorno int
        if (id.equals("main")) {
            type = Type.integerType;
            currentFunction.setType(Type.integerType);
        }

        // Atribuimos o tipo de retorno da função agora, pois utilizaremos para
        // verificar se condiz com o tipo da expressão de retorno
        currentFunction.setReturnType(type);

        // Adiciona a função na tabela global
        symbolTable.putInGlobal("func", id, currentFunction);

        /**
         * Verifica "{" StatList "}"
         */
        if (lexer.token != Symbol.LEFTBRACE) {
            error.signal("{ expected");
        } else {
            lexer.nextToken();

            // StatList
            statList = statList();

            // "}"
            if (lexer.token != Symbol.RIGHTBRACE)
                error.signal("} expected");
        }

        Func func = new Func(id, paramList, type, statList);

        // Atualiza a função na tabela global
        symbolTable.putInGlobal("func", id, func);

        // Limpa a tabela local a cada função
        symbolTable.removeLocalIdent();

        // Verifica se existe retorno da função, caso seja sido declarado um tipo de retorno
        if (type != null && !id.equals("main")) {
            Boolean temRetorno = false;

            for (Stat stat : statList.getStatList()) {
                if (stat.getClass() == ReturnStat.class) {
                    temRetorno = true;
                }
            }

            if (!temRetorno) {
                error.signal("Function "+id+" must return an expression of type "+type.getCname()+"");
            }
        }

        return func;
    }

    // ParamList ::= ParamDec { "," ParamDec }
    public ParamList paramList() {
        ParamList paramList = new ParamList();

        // ParamDec
        paramList.addParam(paramDec());

        // { "," ParamDec }
        while (lexer.token == Symbol.COMMA) {
            lexer.nextToken();
            paramList.addParam(paramDec());
        }

        return paramList;
    }

    // ParamDec ::= Type Id
    public ParamDec paramDec() {

        // Type
        Type type = type();
        // Id
        String id = lexer.getStringValue();
        lexer.nextToken();

        // Adiciona o parâmetro da função na tabela local de variáveis
        Variable var = new Variable(id, type);
        symbolTable.putInLocal("var", id, var);

        return new ParamDec(var);
    }

    // Type ::= "int" | "boolean" | "String"
    private Type type() {
		Type type;

		switch (lexer.token) {
			case INT:
				type = Type.integerType;
				break;
			case BOOLEAN:
				type = Type.booleanType;
				break;
			case STRING:
				type = Type.stringType;
				break;
			default:
				error.signal("Type expected: int, boolean or String");
				type = null;
		}

		lexer.nextToken();

		return type;
	}

    // StatList ::= { Stat }
    public StatList statList() {
        StatList statList = new StatList();

        // { Stat }
        while (lexer.token == Symbol.IDENT || lexer.token == Symbol.LITERALINT || lexer.token == Symbol.TRUE
				|| lexer.token == Symbol.FALSE || lexer.token == Symbol.LITERALSTRING || lexer.token == Symbol.RETURN
				|| lexer.token == Symbol.VAR || lexer.token == Symbol.IF || lexer.token == Symbol.WHILE
				|| lexer.token == Symbol.PRINT || lexer.token == Symbol.PRINTLN) {
			statList.addStat(stat());
		}

        return statList;
    }

    // Stat ::= AssignExprStat | ReturnStat | VarDecStat | IfStat | WhileStat
    public Stat stat() {

        switch (lexer.token) {
			case IDENT: // all are terminals of the assign
			case LITERALINT:
			case TRUE:
			case FALSE:
			case LITERALSTRING:
			case PRINT:
            case PRINTLN:
                // AssignExprStat
                return assignExprStat();
            case READINT:
            case READSTRING:
                break;
            case RETURN:
                // ReturnStat
				return returnStat();
            case VAR:
                // VarDecStat
				return varDecStat();
            case IF:
                // IfStat
				return ifStat();
            case WHILE:
                // WhileStat
				return whileStat();
			default:
				error.signal("Statement expected");
		}

		return null;
    }

    // AssignExprStat ::= Expr [ "=" Expr ] ";"
    public AssignExprStat assignExprStat() {

        /**
         * Análise semântica de atribuições
         */
        Symbol curr = lexer.token;
        if (curr == Symbol.LITERALINT || curr == Symbol.INT || curr == Symbol.BOOLEAN
            || curr == Symbol.STRING || curr == Symbol.LITERALINT
            || curr == Symbol.LITERALSTRING) {
            error.signal("Invalid assignment. "+curr+" cannot receive assignment");
        }

        Expr leftExpr = expr();
        Expr rightExpr = null;

        // [ "=" Expr ] (opcional)
        if (lexer.token == Symbol.ASSIGN) {
            lexer.nextToken();
            rightExpr = expr();
        }

        String currentTokenValue = lexer.getStringValue();

        // Verifica se os dois lados da expressão têm o mesmo tipo
        if (leftExpr != null && rightExpr != null) {
            // Ignora atribuição das funções pre-definidas readInt e readString
            if (!currentTokenValue.matches("readInt|readString")) {
                if (checkAssignment(leftExpr.getType(), rightExpr.getType()) == false) {
                    error.signal("Detected assignment with expressions of different types");
                }
            }
        }

        // ";"
        if (lexer.token != Symbol.SEMICOLON) {
            error.signal("; expected");
        } else {
            lexer.nextToken();
        }

        /**
         * readInt e readString são tipos especiais de atribuição em que
         * não importa o valor da expressão esquerda
         */
        if (currentTokenValue.matches("readInt|readString")) {
            return new AssignExprStat(null, rightExpr);
        }

        return new AssignExprStat(leftExpr, rightExpr);
    }

    /**
     * Verifica se dois Type tem o mesmo tipo
     * @param varType
     * @param exprType
     * @return Boolean
     */
    private boolean checkAssignment(Type varType, Type exprType) {
        if (varType == Type.undefinedType || exprType == Type.undefinedType)
            return true;
        else
            return varType == exprType;
    }

    // ReturnStat ::= "return" Expr ";"
    public ReturnStat returnStat() {
        // "return" é verificado no stat();
        lexer.nextToken();

        // Expr
        Expr expr = expr();

        // Verifica se o tipo de retorno da função condiz com o tipo da expressão
        if (currentFunction.getReturnType() != expr.getType()) {
            error.signal("Function return expression does not have the same type as the function return type");
        }

        // ";"
        if (lexer.token != Symbol.SEMICOLON) {
            error.signal("; expected");
        }

        lexer.nextToken();
        return new ReturnStat(expr);
    }

    // VarDecStat ::= "var" Type Id ";"
    public VarDecStat varDecStat() {
        // "var" é verificado no stat()
        lexer.nextToken();

        // Type
        Type type = type();

        // Id
        if (lexer.token != Symbol.IDENT) {
            error.signal("Id expected");
        }

        // Salva o Id
        String id = lexer.getStringValue();

        // Verifica se o nome da variável não é uma keyword
        if (lexer.isKeyword(id)) {
            error.signal("Variable cannot have the same name of a keyword: " + id);
        }

        lexer.nextToken();

        // ";"
        if (lexer.token != Symbol.SEMICOLON) {
            error.signal("; expected");
        } else {
            lexer.nextToken();
        }

        Variable var = new Variable(id, type);
        VarDecStat varDecstat = new VarDecStat(var);

        // Verifica se a variável já foi declarada
        if (symbolTable.getInLocal("var", id) != null) {
            error.signal("Duplicate variable detected. Variable '"+id+"' has already been declared.");
        } else {
            // Se não foi declarada, adiciona na tabela local
            symbolTable.putInLocal("var", id, var);
        }

        return varDecstat;
    }

    // IfStat ::= "if" Expr "then" StatList [ "else" StatList ] "endif"
    public IfStat ifStat() {
        // "if" é verificado no stat();
        lexer.nextToken();

        // Expr
        Expr expr = expr();

        // "then"
        if (lexer.token != Symbol.THEN) {
            error.signal("'then' expected");
        } else {
            lexer.nextToken();
        }

        // StatList
        StatList leftStatList = statList();
        StatList righStatList = null;

        // [ "else" StatList ] (opcional)
        if (lexer.token == Symbol.ELSE) {
            lexer.nextToken();
            righStatList = statList();
        }

        // "endif"
        if (lexer.token != Symbol.ENDIF) {
            error.signal("'endif' expected");
        } else {
            lexer.nextToken();
        }

        return new IfStat(expr, leftStatList, righStatList);

    }

    // WhileStat ::= "while" Expr "do" StatList "endw"
    public WhileStat whileStat() {
        // "while" é verificado no stat()
        lexer.nextToken();

        // Expr
        Expr expr = expr();

        // "do"
        if (lexer.token != Symbol.DO) {
            error.signal("'do' expected");
        } else {
            lexer.nextToken();
        }

        // StatList
        StatList statList = statList();

        // "endw"
        if (lexer.token != Symbol.ENDW) {
            error.signal("'endw' expected");
        } else {
            lexer.nextToken();
        }

        return new WhileStat(expr, statList);
    }

    // Expr ::= ExprAnd { "||" ExprAnd }
    public Expr expr() {
        ArrayList<ExprAnd> arrayExprAnd = new ArrayList<>();

        // ExprAnd
        arrayExprAnd.add(exprAnd());

        // { "||" ExprAnd }
        while (lexer.token == Symbol.OR) {
            lexer.nextToken();
            arrayExprAnd.add(exprAnd());
        }

        return new Expr(arrayExprAnd);
    }

    // ExprAnd ::= ExprRel { "&&" ExprRel }
    public ExprAnd exprAnd() {
        ArrayList<ExprRel> arrayExprRel = new ArrayList<>();

        // ExprRel
        arrayExprRel.add(exprRel());

        // { "&&" ExprRel }
        while (lexer.token == Symbol.AND) {
            lexer.nextToken();
            arrayExprRel.add(exprRel());
        }

        return new ExprAnd(arrayExprRel);
    }

    // ExprRel ::= ExprAdd [ RelOp ExprAdd ]
    public ExprRel exprRel() {

        // ExprAdd
        ExprAdd leftExprAdd = exprAdd();

        ExprAdd rightExprAdd = null;
        Symbol op = null;

        // [ RelOp ExprAdd ] => RelOp ::= "<" | "<=" | ">" | ">=" | "==" | "!="
		if (lexer.token == Symbol.LT || lexer.token == Symbol.LE || lexer.token == Symbol.GT || lexer.token == Symbol.GE
                || lexer.token == Symbol.EQ || lexer.token == Symbol.NEQ) {
            op = lexer.token;
            lexer.nextToken();
            rightExprAdd = exprAdd();
        }

        return new ExprRel(leftExprAdd, rightExprAdd, op);
    }

    // ExprAdd ::= ExprMult { ( "+" | "-" ) ExprMult }
    public ExprAdd exprAdd() {
        ArrayList<ExprMult> arrayExprMult = new ArrayList<>();
        ArrayList<Symbol> arrayOp = new ArrayList<>();

        // ExprMult
        ExprMult leftExpr = exprMult();
        arrayExprMult.add(leftExpr);

        // { ( "+" | "-" ) ExprMult }
        while (lexer.token == Symbol.PLUS || lexer.token == Symbol.MINUS) {
            Symbol op = lexer.token;
            arrayOp.add(op);
            lexer.nextToken();

            ExprMult rightExpr = exprMult();
            arrayExprMult.add(rightExpr);

            Type leftType = leftExpr.getType();
            Type rightType = rightExpr.getType();

            // Operações de soma e subtração só podem ser feitas com valores inteiros
            if ( ! (leftType == Type.integerType && rightType == Type.integerType ) ) {
                error.signal("Bad operand types for binary operator '"+op+"'");
            }
        }

        return new ExprAdd(arrayExprMult, arrayOp);
    }

    // ExprMult ::= ExprUnary { ( "*" | "/" ) ExprUnary }
    public ExprMult exprMult() {
        ArrayList<ExprUnary> arrayExprUnary = new ArrayList<>();
        ArrayList<Symbol> arrayOp = new ArrayList<>();

        // ExprUnary
        ExprUnary leftExpr = exprUnary();
        arrayExprUnary.add(leftExpr);

        // { ( "*" | "/" ) ExprUnary }
        while (lexer.token == Symbol.MULT || lexer.token == Symbol.DIV) {
            Symbol op = lexer.token;
            arrayOp.add(op);
            lexer.nextToken();

            ExprUnary rightExpr = exprUnary();
            arrayExprUnary.add(rightExpr);

            Type leftType = leftExpr.getType();
            Type rightType = rightExpr.getType();

            // Operações de multiplicação e divisão só podem ser feitas com valores inteiros
            if ( ! (leftType == Type.integerType && rightType == Type.integerType ) ) {
                error.signal("Bad operand types for binary operator '"+op+"'");
            }
        }

        return new ExprMult(arrayExprUnary, arrayOp);
    }

    // ExprUnary ::= [ ( "+" | "-" ) ] ExprPrimary
    public ExprUnary exprUnary() {
        Symbol op = null;

        if (lexer.token == Symbol.PLUS || lexer.token == Symbol.MINUS) {
            op = lexer.token;
            lexer.nextToken();
        }

        ExprPrimary exprPrimary = exprPrimary();

        return new ExprUnary(op, exprPrimary);
    }

    // ExprPrimary ::= Id | FuncCall | ExprLiteral
    public ExprPrimary exprPrimary() {

        switch (lexer.token) {
            case IDENT:
                // salva o id para o FuncCall
                String id = lexer.getStringValue();
                lexer.nextToken();

                if (lexer.token == Symbol.LEFTPAR) {
                    return funcCall(id);
                } else {
                    // Verifica se a variável foi declarada
                    Variable varInTable = (Variable) symbolTable.getInLocal("var", id);
                    if (varInTable == null) {
                        error.signal("Variable '"+id+"' was not declared");
                    }

                    currentVarible = varInTable;
                    return new VariableExpr(varInTable);
				}

            default:
                return exprLiteral();
        }

    }

    // ExprLiteral ::= LiteralInt | LiteralBoolean | LiteralString
    public ExprLiteral exprLiteral() {

        switch (lexer.token) {
			case LITERALINT:
				int number = lexer.getNumberValue();
				lexer.nextToken();
                return new LiteralInt(number);

			case TRUE:
            case FALSE:
				lexer.nextToken();
                return literalBoolean();

			case LITERALSTRING:
				String s = lexer.getStringValue();
				lexer.nextToken();
                return new LiteralString(s);

			default:
				error.signal("ExprLiteral expected");
				return null;
		}

    }

    // LiteralBoolean ::= "true" | "false"
    public LiteralBoolean literalBoolean() {
        if (lexer.token == Symbol.TRUE) {
            return LiteralBoolean.True;
        } else {
            return LiteralBoolean.False;
        }
    }

    // FuncCall ::= Id "(" [ Expr { "," Expr } ] ")"
    public FuncCall funcCall(String id) {
        // Verifica se a função foi declarada
        Func func = (Func) symbolTable.getInGlobal("func", id);
        if (func == null) {
            // Verifica se não é uma função pré-definida
            if (!id.matches("readInt|readString|print|println")) {
                error.signal("Function '"+id+"' has not been declared. Please declare it or move it up in the code.");
            }

            // Inicializa uma função padrão
            func = new Func(id, null, Type.undefinedType, null);
        }

        // funcCall começa com "("
        lexer.nextToken();

        ArrayList<Expr> arrayExpr = new ArrayList<>();

        // [ Expr { "," Expr } ]
        if (
            lexer.token == Symbol.IDENT || lexer.token == Symbol.LITERALINT ||
            lexer.token == Symbol.FALSE || lexer.token == Symbol.TRUE || lexer.token == Symbol.LITERALSTRING
        ) {
            arrayExpr.add(expr());

			while (lexer.token == Symbol.COMMA) {
				lexer.nextToken();
				arrayExpr.add(expr());
			}
        }

        // Verifica se os parâmetros da chamada condizem com a definição da função
        if (func != null && func.getParamList() != null) {
            ArrayList<ParamDec> funcParamList = func.getParamList().getArrayParamDec();
            Integer numOfParamsExpected = funcParamList.size();
            Integer numOfParams = 0;

            // Percorre cada expressão nos parâmetros da chamada da função
            for (int i = 0; i < arrayExpr.size(); i++) {
                Expr expr = arrayExpr.get(i);
                ParamDec expected = funcParamList.get(i);

                // Verifica se os tipos estão corretos
                if (expr.getType() != expected.getVar().getType())
                    error.signal("Function call parameters do not have the correct type.");

                numOfParams++;
            }

            // Verifica se a quantidade de parâmetros condiz
            if (numOfParams != numOfParamsExpected) {
                error.signal("Expected "+numOfParamsExpected+" parameters in function call to '"+id+"', but got "+numOfParams+".");
            }
        }

        // ")"
        if (lexer.token != Symbol.RIGHTPAR) {
            error.signal(") expected");
        } else {
            lexer.nextToken();
        }

        // Marca a variável auxiliar para que o programa saiba
        // que está chamando uma função
        isFuncCall = true;

        if (id.matches("readInt|readString|print|println")) {
            switch (id) {
                case "readInt":
                    return new ReadCall(currentVarible);
                case "readString":
                    return new ReadCall(currentVarible);
                case "print":
                    return new WriteCall(arrayExpr, id, func.getReturnType());
                case "println":
                    return new WriteCall(arrayExpr, id, func.getReturnType());
            }
        }

        return new FuncCall(arrayExpr, id, func.getReturnType());
    }

    /**
     * Variáveis da classe
     */
    private SymbolTable symbolTable;
    private Lexer lexer;
    private CompilerError error;

    // keeps a pointer to the current function being compiled
    private Func currentFunction;

    // apenas para salvar a variável antes do readInt
    private Variable currentVarible;

    private Boolean isFuncCall = false; // variável serve apenas para sinalizar se entrou no funcCall
}
