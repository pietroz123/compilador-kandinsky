package Lexer;

public enum Symbol {
    EOF("eof"),
    IDENT("Id"),
    LEFTPAR("("),
    RIGHTPAR(")"),
    LEFTBRACE("{"),
    RIGHTBRACE("}"),
    ARROW("->"),
    COMMA(","),
    COLON(":"),
    ASSIGN("="),
    SEMICOLON(";"),
    INT("int"),
    BOOLEAN("boolean"),
    STRING("String"),
    LITERALINT("LiteralInt"),
    LITERALSTRING("LiteralString"),
    RETURN("return"),
    VAR("var"),
    IF("if"),
    THEN("then"),
    ELSE("else"),
    ENDIF("endif"),
    WHILE("while"),
    DO("do"),
    ENDW("endw"),
    OR("||"),
    AND ("&&"),
    LT("<"),
    LE("<="),
    GT(">"),
    GE(">="),
    EQ("=="),
    NEQ("!="),
    PLUS("+"),
    MINUS("-"),
    MULT("*"),
    DIV("/"),
    TRUE("true"),
    FALSE("false"),
    PRINT("print"),
    PRINTLN("println"),
    READINT("readInt"),
    READSTRING("readString"),
    DEF("def");

    Symbol(String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }

    public String name;
}