package Lexer;

import java.util.*;
import Error.CompilerError;

public class Lexer {
    public Lexer(char[] input, CompilerError error) {
        this.input = input;
        // add an end-of-file label to make it easy to do the lexer
        input[input.length - 1] = '\0';
        // number of the current line
        lineNumber = 1;
        tokenPos = 0;
        this.error = error;
    }

    // contains the keywords
    static private Hashtable<String, Symbol> keywordsTable;
    // this code will be executed only once for each program execution
    static {
        keywordsTable = new Hashtable<String, Symbol>();
        keywordsTable.put("def", Symbol.DEF);
        keywordsTable.put("int", Symbol.INT);
        keywordsTable.put("boolean", Symbol.BOOLEAN);
        keywordsTable.put("String", Symbol.STRING);
        keywordsTable.put("return", Symbol.RETURN);
        keywordsTable.put("var", Symbol.VAR);
        keywordsTable.put("if", Symbol.IF);
        keywordsTable.put("then", Symbol.THEN);
        keywordsTable.put("else", Symbol.ELSE);
        keywordsTable.put("endif", Symbol.ENDIF);
        keywordsTable.put("while", Symbol.WHILE);
        keywordsTable.put("do", Symbol.DO);
        keywordsTable.put("endw", Symbol.ENDW);
        keywordsTable.put("true", Symbol.TRUE);
        keywordsTable.put("false", Symbol.FALSE);
    }

    /**
     * Verifica se o id atual é uma keyword
     */
    public Boolean isKeyword(String key) {
        return keywordsTable.get(key) != null;
    }


    /**
     * Função para ir para o próximo token
     */
    public void nextToken() {
        char ch;

        // skip empty spaces and other special characters
        while ((ch = input[tokenPos]) == ' ' || ch == '\r' || ch == '\t' || ch == '\n') {
            // count the number of lines
            if (ch == '\n')
                lineNumber++;
            tokenPos++;
        }

        if (ch == '\0')
            token = Symbol.EOF;
        else
        /**
         * skip comments inline
         */
        if (input[tokenPos] == '/' && input[tokenPos + 1] == '/') {

            // comment found
            while (input[tokenPos] != '\0' && input[tokenPos] != '\n')
                tokenPos++;

            nextToken();
        }
        /**
         * skip comment blocks
         */
        else if (input[tokenPos] == '/' && input[tokenPos + 1] == '*') {

            // while not end of comment block ('*/')
            while (input[tokenPos] != '\0' && !(input[tokenPos] == '*' && input[tokenPos + 1] == '/')) {
                // count the number of lines
                if (input[tokenPos] == '\n')
                    lineNumber++;

                tokenPos++;
            }

            if (input[tokenPos] == '*' && input[tokenPos + 1] == '/') {
                tokenPos = tokenPos + 2;
            }

            nextToken();
        }
        /**
         * if not a comment, then its a token
         */
        else {

            /**
             * Se é letra
             */
            if (Character.isLetter(ch)) {
                // get an identifier or keyword
                // StringBuffer represents a string that can grow
                StringBuffer ident = new StringBuffer();

                // is input[tokenPos] a letter ?
                // isLetter is a static method of class Character
                while (Character.isLetter(input[tokenPos]) || Character.isDigit(input[tokenPos])) {
                    // add a character to ident
                    ident.append(input[tokenPos]);
                    tokenPos++;
                }

                // convert a StringBuffer object into a
                // String object
                stringValue = ident.toString();

                // if identStr is in the list of keywords, it is a keyword !
                Symbol value = keywordsTable.get(stringValue);

                // Check if value is a keyword
                if (value == null) {
                    // No
                    token = Symbol.IDENT;
                } else {
                    // Yes
                    token = value;
                }

                while (Character.isDigit(input[tokenPos]))
                    tokenPos++;

            } // end is letter

            /**
             * Se é dígito
             */
            else if (Character.isDigit(ch)) {

                // get a number
                StringBuffer number = new StringBuffer();

                while (Character.isDigit(input[tokenPos])) {
                    number.append(input[tokenPos]);
                    tokenPos++;
                }

                token = Symbol.LITERALINT;

                try {
                    // convert string to int
                    numberValue = Integer.valueOf(number.toString()).intValue();

                } catch (NumberFormatException e) {
                    error.signal("Number out of limits");
                }

                if (numberValue >= MaxValueInteger)
                    error.signal("Number out of limits");

                if (Character.isLetter(input[tokenPos]))
                    error.signal("Number followed by a letter");

                while (Character.isLetter(input[tokenPos])) {
                    tokenPos++;
                }

            } // end is digit

            /**
             * Se não é letra, nem dígito, é um símbolo (caractere especial)
             */
            else {
                tokenPos++;

                switch (ch) {
                    case '+':
                        token = Symbol.PLUS;
                        break;
                    case '-':
                        token = Symbol.MINUS;
                        break;
                    case '*':
                        token = Symbol.MULT;
                        break;
                    case '/':
                        token = Symbol.DIV;
                        break;
                    case '!':
                        if (input[tokenPos] == '=') {
                            tokenPos++;
                            token = Symbol.NEQ;
                        } else {
                            error.signal("Expected = but found: " + ch);
                        }
                        break;
                    case '(':
                        token = Symbol.LEFTPAR;
                        break;
                    case ')':
                        token = Symbol.RIGHTPAR;
                        break;
                    case '{':
                        token = Symbol.LEFTBRACE;
                        break;
                    case '}':
                        token = Symbol.RIGHTBRACE;
                        break;
                    case '=':
                        if (input[tokenPos] == '=') {
                            tokenPos++;
                            token = Symbol.EQ;
                        } else
                            token = Symbol.ASSIGN;
                        break;
                    case ',':
                        token = Symbol.COMMA;
                        break;
                    case ':':
                        token = Symbol.COLON;
                        break;
                    case ';':
                        token = Symbol.SEMICOLON;
                        break;
                    case '<':
                        if (input[tokenPos] == '=') {
                            tokenPos++;
                            token = Symbol.LE;
                        } else
                            token = Symbol.LT;
                        break;
                    case '>':
                        if (input[tokenPos] == '=') {
                            tokenPos++;
                            token = Symbol.GE;
                        } else
                            token = Symbol.GT;
                        break;
                    case '&':
                        if (input[tokenPos] == '&') {
                            tokenPos++;
                            token = Symbol.AND;
                        }
                        else
                            error.signal("Expected & but found: " + ch);
                        break;
                    case '|':
                        if (input[tokenPos] == '|') {
                            tokenPos++;
                            token = Symbol.OR;
                        }
                        else
                            error.signal("Expected | but found: " + ch);
                        break;
                    case '"':
                        /**
                         * Para mensagens entre aspas duplas
                         */
                        StringBuffer ident = new StringBuffer();

                        while (input[tokenPos] != '\0' && input[tokenPos] != '\n' && input[tokenPos] != '"') {
                            ident.append(input[tokenPos]);
                            tokenPos++;
                        }

                        // Valida se está entre aspas duplas
                        if (input[tokenPos] == '"') {
                            token = Symbol.LITERALSTRING;

                            stringValue = ident.toString();
                            tokenPos++;

                        } else if (input[tokenPos] == '\n') {
                            // Houve quebra de linha sem o fim das aspas duplas, invalido
                            error.signal("\" expected");
                            tokenPos++;

                        } else if (input[tokenPos] == '\0') {
                            // Fim do arquivo sem o fim das aspas duplas, invalido
                            error.signal("\" expected");
                            nextToken();
                        }
                        break;

                    default:
                        error.signal("Invalid Character: '" + ch + "'");
                }
            } // end is symbol
        }
        lastTokenPos = tokenPos - 1;

        // System.out.print("current token: " + token);
        // System.out.print(token == Symbol.IDENT ? " - " + getStringValue() : "");
        // System.out.print(token == Symbol.LITERALINT ? " - " + getNumberValue() : "");
        // System.out.println();
    }

    // return the line number of the last token got with getToken()
    public int getLineNumber() {
        return lineNumber;
    }

    public String getCurrentLine() {
        int i = lastTokenPos;
        if (i == 0)
            i = 1;
        else if (i >= input.length)
            i = input.length;
        StringBuffer line = new StringBuffer();
        // go to the beginning of the line
        while (i >= 1 && input[i] != '\n')
            i--;
        if (input[i] == '\n')
            i++;
        // go to the end of the line putting it in variable line
        while (input[i] != '\0' && input[i] != '\n' && input[i] != '\r') {
            line.append(input[i]);
            i++;
        }
        return line.toString();
    }

    public String getStringValue() {
        return stringValue;
    }

    public int getNumberValue() {
        return numberValue;
    }

    public char getCharValue() {
        return charValue;
    }

    // current token
    public Symbol token;
    private String stringValue;
    private int numberValue;
    private char charValue;
    private int tokenPos;

    // input[lastTokenPos] is the last character of the last token
    private int lastTokenPos;

    // program given as input - source code
    private char[] input;

    // number of current line. Starts with 1
    private int lineNumber;

    private CompilerError error;
    private static final int MaxValueInteger = 2147483647;
}