package AuxComp;

import java.util.*;

import AST.Func;
import AST.Variable;

public class SymbolTable {
    private Hashtable<String, Func> globalFuncTable;
    private Hashtable<String, Variable> localVarTable, globalVarTable;

    public SymbolTable() {
        globalFuncTable = new Hashtable<String, Func>();
        globalVarTable = new Hashtable<String, Variable>();
        localVarTable = new Hashtable<String, Variable>();
    }

    public Object putInGlobal(String table, String key, Object value) {
        switch (table) {
            case "var":
                return globalVarTable.put(key, (Variable) value);
            case "func":
                return globalFuncTable.put(key, (Func) value);
            default:
                break;
        }
        return null;
    }

    public Object putInLocal(String table, String key, Object value) {
        switch (table) {
            case "var":
                return localVarTable.put(key, (Variable) value);
            default:
                break;
        }
        return null;
    }

    public Object getInLocal(String table, Object key) {
        switch (table) {
            case "var":
                return localVarTable.get(key);
            default:
                break;
        }
        return null;
    }

    public Object getInGlobal(String table, Object key) {
        switch (table) {
            case "var":
                return globalVarTable.get(key);
            case "func":
                return globalFuncTable.get(key);
            default:
                break;
        }
        return null;
    }

    // public Object get(String key) {
    //     // returns the object corresponding to the key.
    //     Object result;
    //     if ((result = localTable.get(key)) != null) {
    //         // found local identifier
    //         return result;
    //     } else {
    //         // global identifier, if it is in globalTable
    //         return globalTable.get(key);
    //     }
    // }

    public void removeLocalIdent() {
        // remove all local identifiers from the table
        localVarTable.clear();
    }

}