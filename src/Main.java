import AST.*;
import java.io.*;
import java.util.*;

public class Main {

    // // Para Executar multiplos testes ao mesmo tempo
    // static {
    //     try {
    //         for (int i = 1; i <= 11; i++) {
    //             main(new String[] { "src/Testes/Fase2/Corretos/"+i+".in" });
    //         }
    //     } catch (IOException e) {}
    // }

    public static void main( String []args ) throws IOException {

        File file;
        FileReader stream;
        int numChRead;
        Program program;

        Boolean DEBUG = false;

        /**
         * Arquivos de entrada e saída
         */
        String inFileName = args.length > 0 && !args[0].isBlank() ? args[0] : "src/Testes/Fase2/Corretos/2.in";
        String outFileName = args.length > 1 && !args[1].isBlank() ? args[1] : "src/../out.c";

        List<String> arguments = Arrays.asList(inFileName, outFileName);

        if ( arguments.size() != 2 ) {
            System.out.println("Usage:\n Main input output");
            System.out.println("input is the file to be compiled");
            System.out.println("output is the file where the generated code will be stored");
        }
        else {
            file = new File(inFileName);

            if ( ! file.exists() || ! file.canRead() ) {
                System.out.println("Either the file " + inFileName + " does not exist or it cannot be read");
                throw new RuntimeException();
            }

            try {
                stream = new FileReader(file);
            }
            catch ( FileNotFoundException e ) {
                System.out.println("Something wrong: file does not exist anymore");
                throw new RuntimeException();
            }

            // coloca o arquivo de entrada em uma cadeia de char
            // one more character for ’\0’ at the end that will be added by the
            // compiler
            char []input = new char[ (int ) file.length() + 1 ];

            try {
                // número de caracteres
                numChRead = stream.read( input, 0, (int ) file.length() );
            }
            catch ( IOException e ) {
                System.out.println("Error reading file " + inFileName);
                stream.close();
                throw new RuntimeException();
            }

            // if ( numChRead != file.length() ) {
            //     System.out.println("Read error");
            //     stream.close();
            //     throw new RuntimeException();
            // }

            try {
                stream.close();
            }
            catch ( IOException e ) {
                System.out.println("Error in handling the file " + inFileName);
                throw new RuntimeException();
            }

            Compiler compiler = new Compiler();
            FileOutputStream outputStream;

            try {
                outputStream = new FileOutputStream(outFileName);
            }
            catch ( IOException e ) {
                System.out.println("File " + outFileName + " could not be opened for writing");
                throw new RuntimeException();
            }

            PrintWriter printWriter = new PrintWriter(outputStream);
            program = null;
            // the generated code goes to a file and so are the errors

            try {
                // // printa o programa de entrada
                // System.out.print(input);

                if (DEBUG == true) {
                    System.out.println(input);
                }

                try {
                    program = compiler.compile(input, printWriter, inFileName);
                } catch (Exception e) {
                    if (DEBUG == true) {
                        System.out.println("Ocorreu uma exceção ao chamar o compile: " + e.getMessage() + " [" + e.getClass().getCanonicalName() + "]" );
                        System.out.println("Linha: " + e.getStackTrace()[0].getLineNumber());
                        e.printStackTrace();
                    }
                }
            }
            catch ( RuntimeException e ) {
                System.out.println(e);
            }

            if ( program != null ) {
                System.out.println("Program compiled successfully!");

                PW pw = new PW();
                pw.set(printWriter);
                program.genC(pw);

                if ( printWriter.checkError() ) {
                    System.out.println("There was an error in the output");
                }
            }

        }

    } // end main

}